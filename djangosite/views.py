from django.http import HttpResponse

def index(request):
    response = HttpResponse(status=302)
    response['Location'] = '/chat/'
    return response
