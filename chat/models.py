import uuid
from django.db import models


class User(models.Model):
    ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=15)
    display_name = models.CharField(max_length=50)

    def __str__(self):
        return self.username

class Chat(models.Model):
    ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=240)
    users = models.ManyToManyField(User)
    owner = models.UUIDField(default=uuid.uuid4)

    def __str__(self):
        return self.name

class Message(models.Model):
    ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    content = models.CharField(max_length=1000)
    display_name = models.CharField(max_length=200)
    group = models.ForeignKey(Chat, on_delete=models.CASCADE)
    sender = models.UUIDField(default=uuid.uuid4)

    def __str__(self):
        return f"\"{self.content}\""

class File(models.Model):
    ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256) # Max file name length on windows
    expiry = models.DateTimeField('expiry')
    data = models.FileField() #change upload_to

    def __str__(self):
        return self.name
